<?php /* #?ini charset="utf-8"?

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=postgres
Port=
User=
Password=
Database=
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[FileSettings]
VarDir=

[SiteSettings]
SiteName=
SiteURL=
DefaultAccess=ita_sensor
SiteList[]
SiteList[]=ita_sensor
SiteList[]=eng_sensor
SiteList[]=ger_sensor
SiteList[]=backend

[HTTPHeaderSettings]
CustomHeader=disabled
OnlyForAnonymous=enabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=259200, s-maxage=259200

[VarnishSettings]
VarnishHostName=
VarnishPort=
VarnishServers[]

[SearchSettings]
LogSearchStats=disabled

[RoleSettings]
PolicyOmitList[]=user/do_logout
PolicyOmitList[]=exportas/csv
PolicyOmitList[]=exportas/custom
PolicyOmitList[]=ezinfo/is_alive
PolicyOmitList[]=opendata/api

[Session]
SessionNameHandler=custom
SessionNamePerSiteAccess=disabled
CookieSecure=true
CookieHttponly=true
Handler=ezpSessionHandlerPHP
ForceStart=disabled

[SiteAccessSettings]
RemoveSiteAccessIfDefaultAccess=enabled
ForceVirtualHost=true
DebugAccess=enabled
CheckValidity=false
MatchOrder=uri
URIMatchType=map
URIMatchMapItems[]
URIMatchMapItems[]=it;ita_sensor
URIMatchMapItems[]=en;eng_sensor
URIMatchMapItems[]=de;ger_sensor
URIMatchMapItems[]=backend;backend
HostMatchMapItems[]
RelatedSiteAccessList[]
RelatedSiteAccessList[]=ita_sensor
RelatedSiteAccessList[]=eng_sensor
RelatedSiteAccessList[]=ger_sensor
RelatedSiteAccessList[]=backend
AvailableSiteAccessList[]
AvailableSiteAccessList[]=ita_sensor
AvailableSiteAccessList[]=eng_sensor
AvailableSiteAccessList[]=ger_sensor
AvailableSiteAccessList[]=backend
LanguageStaticURI[ita-IT]=/
LanguageStaticURI[eng-GB]=/en
LanguageStaticURI[ger-DE]=/de

[MailSettings]
TransportAlias[smtp]=OpenPASMTPTransport
Transport=smtp
TransportConnectionType=tls
TransportServer=
TransportPort=
TransportUser=
TransportPassword=
AdminEmail=
EmailSender=
SenderHost=
BlackListEmailDomains[]
BlackListEmailDomainSuffixes[]

[InformationCollectionSettings]
EmailReceiver=

[UserSettings]
LogoutRedirect=/?logout
RegistrationEmail=
LoginHandler[]
LoginHandler[]=sensor
LoginHandler[]=ezmbpaex

[TimeZoneSettings]
TimeZone=Europe/Rome

[RegionalSettings]
TextTranslation=enabled
TranslationSA[ita_sensor]=Italiano
TranslationSA[eng_sensor]=English
TranslationSA[ger_sensor]=Deutsch

[DebugSettings]
DebugToolbar=disabled

[UserFormToken]
CookieHttponly=true
CookieSecure=0

*/ ?>

#!/usr/bin/env bash

[[ $DEBUG ]] && set -x

core_list=$(curl -s "http://solr:8983/solr/admin/cores?wt=json")
for k in $(jq '.status| keys | .[]' <<< "$core_list"); do
    core=$(jq ".status[$k]" <<< "$core_list");
    name=$(jq -r '.name' <<< "$core");
    backup_request_url="http://solr:8983/solr/$name/replication?command=backup&location=/opt/ez/backup/$name";
    echo $backup_request_url
    curl -s $backup_request_url
done



# per fare restore: entra in container solr
# stop del core: curl "http://localhost:8983/solr/admin/cores?wt=json&action=UNLOAD&core=$EZ_INSTANCE"
# rimozione indice corrente: rm -rf /opt/ez/ezp-default/data/index
# copia dello snapshot: cp -r /opt/ez/backup/XXX/snapshot.XXX /opt/ez/ezp-default/data/index
# reload del core: curl "http://localhost:8983/solr/admin/cores?wt=json&indexInfo=false&action=CREATE&name=$EZ_INSTANCE&instanceDir=ezp-default&dataDir=data&config=solrconfig.xml&schema=schema.xml&collection=&shard"
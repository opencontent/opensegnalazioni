#!/usr/bin/env bash

container-file-auto-restore "/opt/docker/etc/php/php.webdevops.ini"

echo '' >> /opt/docker/etc/php/php.webdevops.ini
echo '; container env settings' >> /opt/docker/etc/php/php.webdevops.ini

# General php setting
for ENV_VAR in $(envListVars "php\."); do
    env_key=${ENV_VAR#php.}
    env_val=$(envGetValue "$ENV_VAR")

    echo "$env_key = ${env_val}" >> /opt/docker/etc/php/php.webdevops.ini
done


if [[ -n "${PHP_DATE_TIMEZONE+x}" ]]; then
    echo "date.timezone = ${PHP_DATE_TIMEZONE}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_DISPLAY_ERRORS+x}" ]]; then
    echo "display_errors = ${PHP_DISPLAY_ERRORS}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_MEMORY_LIMIT+x}" ]]; then
    echo "memory_limit = ${PHP_MEMORY_LIMIT}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_MAX_EXECUTION_TIME+x}" ]]; then
    echo "max_execution_time = ${PHP_MAX_EXECUTION_TIME}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_POST_MAX_SIZE+x}" ]]; then
    echo "post_max_size = ${PHP_POST_MAX_SIZE}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_UPLOAD_MAX_FILESIZE+x}" ]]; then
    echo "upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_OPCACHE_MEMORY_CONSUMPTION+x}" ]]; then
    echo "opcache.memory_consumption = ${PHP_OPCACHE_MEMORY_CONSUMPTION}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_OPCACHE_MAX_ACCELERATED_FILES+x}" ]]; then
    echo "opcache.max_accelerated_files = ${PHP_OPCACHE_MAX_ACCELERATED_FILES}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_OPCACHE_VALIDATE_TIMESTAMPS+x}" ]]; then
    echo "opcache.validate_timestamps = ${PHP_OPCACHE_VALIDATE_TIMESTAMPS}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_OPCACHE_REVALIDATE_FREQ+x}" ]]; then
    echo "opcache.revalidate_freq = ${PHP_OPCACHE_REVALIDATE_FREQ}" >> /opt/docker/etc/php/php.webdevops.ini
fi

if [[ -n "${PHP_OPCACHE_INTERNED_STRINGS_BUFFER+x}" ]]; then
    echo "opcache.interned_strings_buffer = ${PHP_OPCACHE_INTERNED_STRINGS_BUFFER}" >> /opt/docker/etc/php/php.webdevops.ini
fi

# Workaround for official PHP images
if [[ -n "${PHP_SENDMAIL_PATH+x}" ]]; then
    echo "sendmail_path = ${PHP_SENDMAIL_PATH}" >> /opt/docker/etc/php/php.webdevops.ini
fi

# Disable all PHP mods specified in PHP_DISMOD as comma separated list
if [[ -n "${PHP_DISMOD+x}" ]]; then
    ini_dir_cli=$(php -i | grep 'Scan this dir for additional .ini files' | cut -c44-)
    ini_dir_fpm=$(php-fpm -i | grep 'Scan this dir for additional .ini files' | cut -c44-)
    for DISABLE_MOD in ${PHP_DISMOD//,/ }; do
        rm -f ${ini_dir_cli}/*${DISABLE_MOD}*
        rm -f ${ini_dir_fpm}/*${DISABLE_MOD}*
    done
fi

# Link composer version accordingly
ln -sf /usr/local/bin/composer${COMPOSER_VERSION:-2} /usr/local/bin/composer



if [[ -z $EZ_ROOT ]]; then
    echo "[error] EZ_ROOT is empty this variable is required in this container, please set it to the public dir of Ez and restart"
    exit 1
else
    echo "[info] Current root is ${EZ_ROOT}"
fi

EZ_USER='application'
EZ_USER_GROUP='application'
EZ_CHMOD_VAR=${EZ_CHMOD_VAR:-'2775'}
EZ_CHMOD_LOG=${EZ_CHMOD_LOG:-'660'}
EZ_CLUSTER_GET_OWNERSHIP=${EZ_CLUSTER_GET_OWNERSHIP:-'true'}

if [ ! -d ${EZ_ROOT}/var/cache/ini ]; then
    mkdir -p ${EZ_ROOT}/var/cache/ini
    echo "[info] created var/cache/ini"
fi

if [ ! -d ${EZ_ROOT}/var/cache/texttoimage ]; then
    mkdir -p ${EZ_ROOT}/var/cache/texttoimage
    echo "[info] created var/cache/texttoimage"
fi

if [ ! -d ${EZ_ROOT}/var/cache/codepages ]; then
    mkdir -p ${EZ_ROOT}/var/cache/codepages
    echo "[info] created var/cache/codepages"
fi

if [ ! -d ${EZ_ROOT}/var/cache/translation ]; then
    mkdir -p ${EZ_ROOT}/var/cache/translation
    echo "[info] created var/cache/translation"
fi

if [ ! -d ${EZ_ROOT}/var/log ]; then
    mkdir -p ${EZ_ROOT}/var/log
    echo "[info] created dir var/log"
fi

if [ ! -d ${EZ_ROOT}/var/log/storage ]; then
    mkdir -p ${EZ_ROOT}/var/log/storage
    echo "[info] created dir var/log"
fi

for logfile in cluster_error debug error ocfoshttpcache storage warning strict notice; do
  if [[ ! -f ${EZ_ROOT}/var/log/${logfile}.log ]]; then
    touch ${EZ_ROOT}/var/log/${logfile}.log && \
    chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var/log/${logfile}.log
  fi
  tail -F ${EZ_ROOT}/var/log/${logfile}.log &
done

if [[ -n $EZ_INSTANCE ]]; then
  if [[ ! -f ${EZ_ROOT}/var/${EZ_INSTANCE}/log/sensor.log ]]; then
    touch ${EZ_ROOT}/var/${EZ_INSTANCE}/log/sensor.log && \
    chown www-data ${EZ_ROOT}/var/${EZ_INSTANCE}/log/sensor.log
  fi
  tail -F ${EZ_ROOT}/var/${EZ_INSTANCE}/log/sensor.log &
fi

#Permesso di scrittura per '$EZ_USER_GROUP' per la directory var/
echo "[info] chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var"
      chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var
      chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var/*
      chown $EZ_USER.$EZ_USER_GROUP ${EZ_ROOT}/var/cache/*
echo "[info] chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var"
      chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var
      chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var/*
      chmod ${EZ_CHMOD_VAR} ${EZ_ROOT}/var/cache/*

# aumenta la sicurezza dando 660 ai file di log
echo "[info] chmod ${EZ_CHMOD_LOG} ${EZ_ROOT}/var/log/*"
     chmod ${EZ_CHMOD_LOG} ${EZ_ROOT}/var/log/*

echo "[info] chown -R -L $EZ_USER.$EZ_USER_GROUP /var/www/installer"
      chown -R $EZ_USER.$EZ_USER_GROUP /var/www/installer

if [[ -n $EZINI_file__eZDFSClusteringSettings__MountPointPath ]]; then
    if [[ -d $EZINI_file__eZDFSClusteringSettings__MountPointPath ]]; then
        echo "[info] fixing perms in '${EZINI_file__eZDFSClusteringSettings__MountPointPath}' ..."
        chown $EZ_USER $EZINI_file__eZDFSClusteringSettings__MountPointPath
    else
        mkdir -p $EZINI_file__eZDFSClusteringSettings__MountPointPath
        chown $EZ_USER $EZINI_file__eZDFSClusteringSettings__MountPointPath
    fi
fi

TRUNCATE_CACHE_TABLE=${TRUNCATE_CACHE_TABLE:-'false'}
if [[ $TRUNCATE_CACHE_TABLE == 'true' ]]; then
    echo "[info] truncate cache dfs table"
    php extension/openpa/bin/clustering/truncate_ezdfs_cache.php --allow-root-user
else
    echo "[info] TRUNCATE_CACHE_TABLE is missing or set to false"
fi

RUN_INSTALLER=${RUN_INSTALLER:-'true'}
RUN_INSTALLER_REPORTS=${RUN_INSTALLER_REPORTS:-'false'}
RUN_REINDEX_CONTENT=${RUN_REINDEX_CONTENT:-'false'}
if [[ -n $EZ_INSTANCE ]]; then
    if [[ -f vendor/bin/ocinstall ]]; then
        if [[ $RUN_INSTALLER == 'true' ]]; then

            echo "[info] ensure pgcrypto is available"
              php vendor/opencontent/ocinstaller/bin/install_pgcrypto.php --host=${EZINI_site__DatabaseSettings__Server} --port=${EZINI_site__DatabaseSettings__Port} --user=${EZINI_site__DatabaseSettings__User} --password=${EZINI_site__DatabaseSettings__Password} --database=${EZINI_site__DatabaseSettings__Database}  || exit 2

            echo "[info] run installer on ${EZ_INSTANCE}"
              php vendor/bin/ocinstall --allow-root-user -v -sbackend --embed-dfs-schema --no-interaction ../installer/
        else
            echo "[info] RUN_INSTALLER is set to false (install only base schema)"
              php vendor/bin/ocinstall --allow-root-user -sbackend --embed-dfs-schema --no-interaction --languages=ita-IT,ita-PA --only-schema ../installer/
        fi
        if [[ $RUN_INSTALLER_REPORTS == 'true' ]]; then
              if [[ -n $EZ_INSTANCE ]]; then
                  echo "[info] run reports installer on ${EZ_INSTANCE}"
                  php vendor/bin/ocinstall --allow-root-user -v -sbackend --no-interaction ../installer/modules/reports
              fi
        else
            echo "[info] RUN_INSTALLER_REPORTS is missing or set to false"
        fi
    else
        echo "[warning] Installer bin vendor/bin/ocinstall not found"
    fi

    if [[ $RUN_REINDEX_CONTENT == 'true' ]]; then
          echo "[info] run reindex content on ${EZ_INSTANCE}"
          php bin/php/updatesearchindex.php -sbackend --clean --allow-root-user
    else
          echo "[info] RUN_REINDEX_CONTENT is set to false"
    fi

    if [[ -f extension/ocsensor/bin/php/update_translations.php ]]; then
        echo "[info] Update translations"
        php extension/ocsensor/bin/php/update_translations.php
    else
        echo "[warning] Script extension/ocsensor/bin/php/update_translations.php not found"
    fi

    echo "[info] Clear all cache"
    php bin/php/ezcache.php --clear-all -d

else
    echo "[warning] EZ_INSTANCE not found"
fi